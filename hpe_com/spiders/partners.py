# -*- coding: utf-8 -*-
import json
import scrapy

from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode


class PartnersSpider(scrapy.Spider):
    name = 'partners'
    allowed_domains = ['hpe.com']

    limit = 10000 # default: 250

    partner_types = ['partner_reseller', 'partner_service_prov', 'partner_replacement_parts', 'partner_auth_suprt_prov']

    states = [
        'AL',
        'DZ',
        'AD',
        'AO',
        'AG',
        'AR',
        'AM',
        'AW',
        'AU',
        'AT',
        'AZ',
        'BS',
        'BH',
        'BD',
        'BB',
        'BY',
        'BE',
        'BZ',
        'BJ',
        'BM',
        'BO',
        'BA',
        'BW',
        'BR',
        'BN',
        'BG',
        'BF',
        'KH',
        'CM',
        'CA',
        'KY',
        'CL',
        'CN',
        'HK',
        'MO',
        'TW',
        'CO',
        'KM',
        'CG',
        'CR',
        'CI',
        'HR',
        'CY',
        'CZ',
        'CD',
        'DK',
        'DO',
        'TL',
        'EC',
        'EG',
        'SV',
        'GQ',
        'EE',
        'ET',
        'FO',
        'FJ',
        'FI',
        'FR',
        'PF',
        'GA',
        'GE',
        'DE',
        'GH',
        'GI',
        'GR',
        'GL',
        'GP',
        'GT',
        'GN',
        'GY',
        'HT',
        'HN',
        'HU',
        'IS',
        'IN',
        'ID',
        'IQ',
        'IE',
        'IL',
        'IT',
        'JM',
        'JO',
        'KZ',
        'KE',
        'KV',
        'KW',
        'KG',
        'LA',
        'LV',
        'LB',
        'LS',
        'LY',
        'LI',
        'LT',
        'LU',
        'MG',
        'MW',
        'MY',
        'MV',
        'ML',
        'MT',
        'MQ',
        'MU',
        'YT',
        'MX',
        'MD',
        'MC',
        'MN',
        'ME',
        'MA',
        'MZ',
        'MM',
        'NA',
        'NP',
        'NL',
        'AN',
        'NC',
        'NZ',
        'NI',
        'NE',
        'NG',
        'MK',
        'NO',
        'OM',
        'PK',
        'PA',
        'PG',
        'PY',
        'PE',
        'PH',
        'PL',
        'PT',
        'PR',
        'QA',
        'RE',
        'RO',
        'RU',
        'RW',
        'KN',
        'LC',
        'SM',
        'SA',
        'SN',
        'RS',
        'SG',
        'SK',
        'SI',
        'ZA',
        'KR',
        'ES',
        'LK',
        'SR',
        'SZ',
        'SE',
        'CH',
        'TJ',
        'TZ',
        'TH',
        'TG',
        'TT',
        'TN',
        'TR',
        'TM',
        'TC',
        'UG',
        'UA',
        'AE',
        'UK',
        'US',
        'UY',
        'UZ',
        'VU',
        'VE',
        'VN',
        'VI',
        'YE',
        'ZM',
        'ZW',
    ]

    url = 'https://findapartner.hpe.com/rest/getlist?lang=en_US'

    headers = {
        'Accept-Language': 'en-US,en;q=0.9,ru;q=0.8,ru-RU;q=0.7,ja;q=0.6,pt;q=0.5,sv;q=0.4',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Content-Type': 'application/json',
        'Origin': 'https://findapartner.hpe.com',
        'Referer': 'https://findapartner.hpe.com/',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    payload = '{"request":{"appkey":"94F4F2F8-1F26-11E9-89FA-ADAF55A65BB0","formdata":{"objectname":"Locator::Base","currency":"US","where":{"country":{"eq":"%s"},"parent_partner_id":{"eq":"null"},"%s":{"eq":1}},"order":"partner_ranking \u0021= \'MASTER AREA\', partner_ranking \u0021= \'PLATINUM\', partner_ranking \u0021= \'GOLD\', partner_ranking \u0021= \'SILVER\', partner_ranking \u0021= \'BUSINESS\'","limit":%s}}}'


    def start_requests(self):
        for state in self.states:
            for partner_type in self.partner_types:
                payload = self.payload % (state, partner_type, self.limit)

                yield scrapy.Request(method='POST', url=self.url, headers=self.headers, body=payload, meta={'payload': payload}, callback=self.parse, dont_filter=True)


    def parse(self, response):
        # inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json', exc_info=True)
            self.logger.debug(response.text)
            return

        yield from data.get('response', {}).get('collection', [])
