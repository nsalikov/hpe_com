# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem


class PartnerDuplicatesPipeline(object):

    seen = set()

    def process_item(self, item, spider):
        key = item['uid']

        if key in self.seen:
            raise DropItem("Duplicate: {}:".format(key))
        else:
            self.seen.add(key)

        return item
